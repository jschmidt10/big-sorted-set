package com.github.jschmidt10.bss.e2e;

import com.github.jschmidt10.bss.BigSortedSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Stream;

public class Runner {

    private static final Logger LOGGER = LogManager.getLogger(Runner.class);

    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            throw new IllegalArgumentException("Expects two arguments: <movie filename> <sorted set work dir>");
        }

        Path inputPath = Paths.get(args[0]);
        if (!Files.exists(inputPath) || !Files.isReadable(inputPath)) {
            throw new IllegalArgumentException("File " + inputPath + " is not readable or does not exist.");
        }

        Path workdir = Paths.get(args[1]);
        if (!Files.isWritable(workdir)) {
            throw new IllegalArgumentException("Workdir " + workdir + " is not writable.");
        }

        LOGGER.info("Populating movies from {}", inputPath);

        Comparator<Movie> comparator = Comparator.comparing(Movie::getTitle);
        BigSortedSet<Movie> movieSet = new BigSortedSet.Builder<Movie>()
                .bufferSize(5000)
                .comparator(comparator)
                .maxSpillFiles(6)
                .minFilesToCompact(3)
                .workDir(workdir)
                .serializer(new MovieLineSerializer())
                .build();

        try (Stream<String> lines = Files.lines(inputPath)) {
            lines.filter(line -> !line.startsWith("movieId")).map(Movie::fromCsvLine).forEach(movie -> {
                try {
                    movieSet.add(movie);
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        LOGGER.info("Finished populating {} movies", movieSet.size());

        LOGGER.info("Validating sorted set is sorted");

        Movie last = null;

        for (Movie movie : movieSet) {
            if (last != null && comparator.compare(last, movie) > 0) {
                throw new IllegalStateException("Found out of order movies. Last: " + last + ", Current: " + movie);
            }

            last = movie;
        }

        LOGGER.info("Finished");

        movieSet.clear();

        LOGGER.info("Cleaned up");
    }
}
