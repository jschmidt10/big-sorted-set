package com.github.jschmidt10.bss.e2e;

import com.github.jschmidt10.bss.SFileSerializer;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MovieSerializer implements SFileSerializer<Movie> {

    @Override
    public void write(Movie movie, DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(movie.getId());
        dataOutput.writeUTF(movie.getTitle());
        dataOutput.writeUTF(movie.getGenres());
    }

    @Override
    public Movie read(DataInput dataInput) throws IOException {
        int id = dataInput.readInt();
        String title = dataInput.readUTF();
        String genres = dataInput.readUTF();

        return new Movie(id, title, genres);
    }
}
