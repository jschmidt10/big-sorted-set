package com.github.jschmidt10.bss.e2e;

public final class Movie {

    private final int id;
    private final String title;
    private final String genres;

    public Movie(int id, String title, String genres) {
        this.id = id;
        this.title = title;
        this.genres = genres;
    }

    public static Movie fromCsvLine(String line) {
        int firstComma = line.indexOf(',');
        int lastComma = line.lastIndexOf(',');

        if (firstComma < 0 || lastComma < 0) {
            throw new IllegalArgumentException("Invalid line. Expected three columns: <id> <title> <genres>. Received: " + line);
        }

        String id = line.substring(0, firstComma);
        String title = line.substring(firstComma + 1, lastComma);
        String genres = line.substring(lastComma + 1);

        if (title.startsWith("\"") && title.endsWith("\"")) {
            title = title.substring(1, title.length() - 1);
        }

        return new Movie(Integer.parseInt(id), title, genres);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenres() {
        return genres;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", genres='" + genres + '\'' +
                '}';
    }
}
