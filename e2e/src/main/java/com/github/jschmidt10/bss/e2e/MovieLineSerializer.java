package com.github.jschmidt10.bss.e2e;

import com.github.jschmidt10.bss.SFileSerializer;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MovieLineSerializer implements SFileSerializer<Movie> {

    @Override
    public void write(Movie movie, DataOutput dataOutput) throws IOException {
        String str = movie.getId() + "\0" + movie.getTitle() + "\0" + movie.getGenres() + "\n";
        dataOutput.writeUTF(str);
    }

    @Override
    public Movie read(DataInput dataInput) throws IOException {
        String str = dataInput.readUTF();
        String[] tokens = str.split("\0");
        return new Movie(Integer.parseInt(tokens[0]), tokens[1], tokens[2]);
    }
}
