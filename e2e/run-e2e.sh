#!/usr/bin/env bash

set -o errexit
set -o pipefail

err_report() {
  echo "errexit on line $(caller)" >&2
}

trap err_report ERR

declare basedir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd ${basedir}/target

if ! ls big-sorted-set-*-jar-with-dependencies.jar > /dev/null
then
  echo "You must build e2e tests first"
  exit 1
fi

if [[ ! -f "ml-25m.zip" ]]
then
  echo "Downloading MovieLens dataset"
  curl --silent --output ml-25m.zip http://files.grouplens.org/datasets/movielens/ml-25m.zip
fi

echo "Unpacking MovieLens dataset"
unzip -u ml-25m.zip

declare workdir=$(mktemp -d)

echo "Launching test run"
java -jar big-sorted-set-*-jar-with-dependencies.jar "ml-25m/movies.csv" "${workdir}"