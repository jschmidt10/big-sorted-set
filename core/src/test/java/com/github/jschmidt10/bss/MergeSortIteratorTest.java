package com.github.jschmidt10.bss;

import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MergeSortIteratorTest {

    @Test
    public void shouldMergeSortMultipleIteratorsWithNoDuplication() {
        SortedSet<Integer> set1 = new TreeSet<>(Arrays.asList(1, 2));
        SortedSet<Integer> set2 = new TreeSet<>(Arrays.asList(2, 3, 4));
        SortedSet<Integer> set3 = new TreeSet<>(Arrays.asList(4, 5, 6, 7));

        MergeSortIterator<Integer> mergedIter = new MergeSortIterator<>(Comparator.naturalOrder(),
                Arrays.asList(set1.iterator(), set2.iterator(), set3.iterator()));

        int expected = 1;
        while (mergedIter.hasNext()) {
            assertThat(mergedIter.next(), is(expected++));
        }

        assertThat(expected, is(8));
    }

    @Test
    public void shouldDeduplicateAcrossIterations() {
        List<Integer> list1 = Arrays.asList(1, 1, 1, 2);
        List<Integer> list2 = Arrays.asList(1, 1, 2, 2, 2);
        List<Integer> list3 = Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 2);

        MergeSortIterator<Integer> mergedIter = new MergeSortIterator<>(Comparator.naturalOrder(),
                Arrays.asList(list1.iterator(), list2.iterator(), list3.iterator()));

        List<Integer> result = new ArrayList<>();
        while (mergedIter.hasNext()) {
            result.add(mergedIter.next());
        }

        assertThat(result, is(Arrays.asList(1, 2)));
    }
}
