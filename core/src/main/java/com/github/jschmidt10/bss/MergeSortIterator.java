package com.github.jschmidt10.bss;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * An Iterator that merges multiple sorted iterators together while de-duplicating.
 * @param <T>
 */
public class MergeSortIterator<T> implements Iterator<T> {

    private final Comparator<T> comparator;
    private final List<PeekingIterator<T>> iters;
    private final Consumer<T> elementDroppedCallback;

    private T next;

    public MergeSortIterator(Comparator<T> comparator, List<Iterator<T>> iters) {
        this(comparator, iters, null);
    }

    public MergeSortIterator(Comparator<T> comparator, List<Iterator<T>> iters, Consumer<T> elementDroppedCallback) {
        this.comparator = comparator;
        this.iters = iters.stream().map(PeekingIterator::new).collect(Collectors.toList());
        this.elementDroppedCallback = elementDroppedCallback;
    }

    @Override
    public boolean hasNext() {
        if (next == null) {
            T min = null;

            for (PeekingIterator<T> iter : iters) {
                T peeked = iter.peek();
                if (peeked != null) {
                    if (min == null || comparator.compare(peeked, min) < 0) {
                        min = peeked;
                    }
                }
            }

            if (min != null) {
                next = min;

                boolean first = true;
                for (PeekingIterator<T> iter : iters) {
                    while (iter.peek() != null && comparator.compare(next, iter.peek()) == 0) {
                        if (elementDroppedCallback != null) {
                            if (first) {
                                first = false;
                            }
                            else {
                                elementDroppedCallback.accept(iter.peek());
                            }
                        }
                        iter.next();
                    }
                }
            }
        }
        return next != null;
    }

    @Override
    public T next() {
        T result = next;
        next = null;
        return result;
    }
}
